build:
	docker compose build

up:
	docker compose up -d
start:
	make up

down:
	docker compose down
stop:
	make down

init:
	docker compose exec -u root zf3-seo composer install -n
	docker compose exec -u root zf3-seo chown -R www-data:www-data vendor

sh:
	docker compose exec zf3-seo sh
sh-root:
	docker compose exec -u root zf3-seo sh

install:
	make build
	make up
	make init
