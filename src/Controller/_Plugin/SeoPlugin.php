<?php
declare(strict_types=1);
namespace Zf3Lib\Seo\Controller\_Plugin;

use Zf3Lib\Seo\Service\Seo\SeoManager;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

class SeoPlugin extends AbstractPlugin
{
    private SeoManager $seoManager;

    public function __construct(SeoManager $seoManager)
    {
        $this->seoManager = $seoManager;
    }

    public function __invoke(): SeoManager
    {
        return $this->seoManager;
    }
}