<?php
declare(strict_types=1);
namespace Zf3Lib\Seo\Controller;

use Zf3Lib\Lib\Controller\Controller as AbstractController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractController
{
    public function robotsTxtAction(): ViewModel
    {
        $host = $_SERVER['HTTP_HOST'] ?? '';

        $this->layout()->setTemplate('layout/blank');
        return new ViewModel([
            'host' => $host
        ]);
    }
}