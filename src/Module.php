<?php
declare(strict_types=1);
namespace Zf3Lib\Seo;

use Zf3Lib\Lib\Translator\Translator;
use Zf3Lib\Lib\Translator\TranslatorServiceFactory;
use Zf3Lib\Lib\View\Helper\LocalDateFormatted;
use Zf3Lib\Lib\View\Helper\ProjectName;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Zf3Lib\Seo\Service\Seo\SeoManager;
use Zf3Lib\Seo\View\Helper\Seo;
use JetBrains\PhpStorm\ArrayShape;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getControllerConfig(): array
    {
        return [
            'factories' => [
                Controller\IndexController::class => function(ServiceManager $manager) { return new Controller\IndexController($manager); },
            ],
        ];
    }

    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getControllerPluginConfig(): array
    {
        return [
            'factories' => [
                'seo' =>
                    fn (ServiceManager $sm) =>
                        new Controller\_Plugin\SeoPlugin(
                            $sm->get(SeoManager::class),
                        ),
            ],
        ];
    }

    #[ArrayShape(['factories' => "array"])]
    public function getServiceConfig(): array
    {
        return [
            'factories' => [
                SeoManager::class => function (ServiceManager $serviceManager) { return new SeoManager($serviceManager); },
                Translator::class => TranslatorServiceFactory::class,
            ],
        ];
    }

    #[ArrayShape(['invokables' => "string[]", 'factories' => "\Closure[]", 'aliases' => "string[]"])]
    public function getViewHelperConfig(): array
    {
        return [
            'invokables' => [
                'projectName' => ProjectName::class,
                'localDateFormatted' => LocalDateFormatted::class,
            ],
            'factories' => [
                Seo::class => fn (ServiceManager $sm) => new Seo($sm->get(SeoManager::class)),
                'translate' => function (ServiceManager $serviceManager) { return new \Zf3Lib\Lib\View\Helper\Translate($serviceManager); },
                'locale' => function (ServiceManager $serviceManager) { return new \Zf3Lib\Lib\View\Helper\Locale($serviceManager); },
            ],
            'aliases' => [
                'seo' => Seo::class,
            ],
        ];
    }

    public function onBootstrap(MvcEvent $event): void
    {
        $event
            ->getApplication()
            ->getEventManager()
            ->attach(MvcEvent::EVENT_DISPATCH, [$this, 'initSeoManager'], 1);
    }

    public function initSeoManager(MvcEvent $event): void
    {
        /** @var SeoManager $seoManager */
        try {
            $seoManager = $event->getApplication()->getServiceManager()->get(SeoManager::class);
            $seoManager->initByRoute();
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {}
    }
}
