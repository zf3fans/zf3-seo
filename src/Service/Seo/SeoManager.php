<?php
declare(strict_types=1);
namespace Zf3Lib\Seo\Service\Seo;

use JetBrains\PhpStorm\Pure;
use Laminas\Mvc\Application;
use Zf3Lib\Lib\Translator\Translator;
use Laminas\ServiceManager\ServiceManager;

class SeoManager
{
    private ServiceManager $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public const PARAM_TITLE       = 'title';
    public const PARAM_H1          = 'h1';
    public const PARAM_DESCRIPTION = 'description';
    public const PARAM_KEYWORDS    = 'keywords';

    public const PARAMS = [
        self::PARAM_TITLE,
        self::PARAM_H1,
        self::PARAM_DESCRIPTION,
        self::PARAM_KEYWORDS,
    ];

    private static array $params = [];

    public function initByRoute(): void
    {
        /** @var Application $app */
        $app = $this->serviceManager->get('Application');
        $routeMatch = $app->getMvcEvent()->getRouteMatch();

        if ($routeMatch === null) {
            return;
        }

        $routeName   = $routeMatch->getMatchedRouteName();
        $routeParams = $routeMatch->getParams();

        foreach (['controller', 'action'] as $key) {
            unset($routeParams[$key]);
        }

        $page = $this->getPage($routeName, $routeParams, $this->getLocale());
        $this->setParams($page);
    }

    private function getLocale(): string
    {
        /** @var Translator $translator */
        $translator = $this->serviceManager->get(Translator::class);
        return $translator->getLocale();
    }

    private function getPage(string $routeName, array $routeParams, string $locale): ?array
    {
        $page = null;
        $seo_pages = $this->serviceManager->get('config')['seo_pages'] ?? [];
        foreach ($seo_pages as $seo_page) {
            if (
                   (($seo_page['slug'] ?? '') === $routeName)
                && (($seo_page['locale'] ?? '') === $locale)
                && (count(array_diff($seo_pages['params'] ?? [], $routeParams)) === 0)
            ) {
                $page = $seo_page;
                break;
            }
        }

        return $page;
    }

    private function checkParam(string $param): bool
    {
        return in_array($param, self::PARAMS, true);
    }

    public function set(string $param, string $value): void
    {
        if (!$this->checkParam($param)) {
            return;
        }

        self::$params[$param] = $value;
    }

    #[Pure]
    public function get(string $param): ?string
    {
        if (!$this->checkParam($param)) {
            return null;
        }

        return self::$params[$param] ?? '';
    }

    /**
     * @param ?array $params
     */
    public function setParams(?array $params): void
    {
        if ($params === null) {
            return;
        }

        foreach ($params as $param => $value) {
            if (is_array($value)) {
                continue;
            }
            $this->set((string) $param, (string) $value);
        }
    }



    // region Particular Params

    #[Pure]
    public function h1(string $default = ''): string
    {
        return $this->get(self::PARAM_H1) ?? $default;
    }

    #[Pure]
    public function title(string $default = ''): string
    {
        return $this->get(self::PARAM_TITLE) ?? $default;
    }

    #[Pure]
    public function keywords(string $default = ''): string
    {
        return $this->get(self::PARAM_KEYWORDS) ?? $default;
    }

    #[Pure]
    public function description(string $default = ''): string
    {
        return $this->get(self::PARAM_DESCRIPTION) ?? $default;
    }

    // endregion Particular Params
}