<?php
declare(strict_types=1);
namespace Zf3Lib\Seo\View\Helper;

use Zf3Lib\Seo\Service\Seo\SeoManager;
use Laminas\ServiceManager\ServiceManager;
use Laminas\View\Helper\AbstractHelper;

class Seo extends AbstractHelper
{
    private SeoManager $seoManager;

    public function __construct(SeoManager $seoManager)
    {
        $this->seoManager = $seoManager;
    }

    public function __invoke(): SeoManager
    {
        return $this->seoManager;
    }
}