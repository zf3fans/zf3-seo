<?php
declare(strict_types=1);
namespace Zf3Lib\Seo;

use Laminas\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'robots-txt' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/robots.txt',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'robots-txt',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'zf3-lib/seo/index/robots-txt'  => __DIR__ . '/../view/seo/index/robots-txt.phtml',
        ],
        'template_path_stack' => [
            'seo' => __DIR__ . '/../view',
        ],
    ],
];
